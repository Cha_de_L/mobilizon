## `msgid`s in this file come from POT (.pot) files.
##
## Do not add, change, or remove `msgid`s manually here as
## they're tied to the ones in the corresponding POT file
## (with the same domain).
##
## Use `mix gettext.extract --merge` or `mix gettext.merge`
## to merge POT files into PO files.
msgid ""
msgstr ""
"PO-Revision-Date: 2020-10-29 08:17+0000\n"
"Last-Translator: Berto Te <ateira@3fpj.com>\n"
"Language-Team: Spanish <https://weblate.framasoft.org/projects/mobilizon/"
"backend-errors/es/>\n"
"Language: es\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.1\n"

msgid "can't be blank"
msgstr "no pueden estar vacíos"

msgid "has already been taken"
msgstr "ya se ha tomado"

msgid "is invalid"
msgstr "no es válido"

msgid "must be accepted"
msgstr "debe ser aceptado"

msgid "has invalid format"
msgstr "tiene formato no válido"

msgid "has an invalid entry"
msgstr "tiene una entrada inválida"

msgid "is reserved"
msgstr "Está reservado"

msgid "does not match confirmation"
msgstr "la confirmación no coincide"

msgid "is still associated with this entry"
msgstr "todavía está asociado con esta entrada"

msgid "are still associated with this entry"
msgstr "todavía están asociados con esta entrada"

msgid "should be %{count} character(s)"
msgid_plural "should be %{count} character(s)"
msgstr[0] "debe tener un carácter"
msgstr[1] "debe tener% {count} caracteres"

msgid "should have %{count} item(s)"
msgid_plural "should have %{count} item(s)"
msgstr[0] "debería tener un artículo"
msgstr[1] "debería tener% {count} artículos"

msgid "should be at least %{count} character(s)"
msgid_plural "should be at least %{count} character(s)"
msgstr[0] "debe tener al menos un carácter"
msgstr[1] "debe tener al menos% {count} caracteres"

msgid "should have at least %{count} item(s)"
msgid_plural "should have at least %{count} item(s)"
msgstr[0] "debe tener al menos un elemento"
msgstr[1] "debe tener al menos% {count} elementos"

msgid "should be at most %{count} character(s)"
msgid_plural "should be at most %{count} character(s)"
msgstr[0] "debe tener como máximo un carácter"
msgstr[1] "debe tener como máximo% {count} caracteres"

msgid "should have at most %{count} item(s)"
msgid_plural "should have at most %{count} item(s)"
msgstr[0] "debe tener como máximo un artículo"
msgstr[1] "debe tener como máximo% {count} artículos"

msgid "must be less than %{number}"
msgstr "debe ser inferior a% {number}"

msgid "must be greater than %{number}"
msgstr "debe ser mayor que% {number}"

msgid "must be less than or equal to %{number}"
msgstr "debe ser menor o igual que% {number}"

msgid "must be greater than or equal to %{number}"
msgstr "debe ser mayor o igual que% {number}"

msgid "must be equal to %{number}"
msgstr "debe ser igual a% {number}"

#: lib/graphql/resolvers/user.ex:103
#, elixir-format
msgid "Cannot refresh the token"
msgstr "No se puede actualizar el token"

#: lib/graphql/resolvers/group.ex:139
#, elixir-format
msgid "Creator profile is not owned by the current user"
msgstr "El perfil del creador no es propiedad del usuario actual"

#: lib/graphql/resolvers/group.ex:203
#, elixir-format
msgid "Current profile is not a member of this group"
msgstr "El perfil actual no es miembro de este grupo"

#: lib/graphql/resolvers/group.ex:207
#, elixir-format
msgid "Current profile is not an administrator of the selected group"
msgstr "El perfil actual no es un administrador del grupo seleccionado"

#: lib/graphql/resolvers/user.ex:512
#, elixir-format
msgid "Error while saving user settings"
msgstr "Error al guardar los parámetros del usuario"

#: lib/graphql/error.ex:90 lib/graphql/resolvers/group.ex:200
#: lib/graphql/resolvers/group.ex:248 lib/graphql/resolvers/group.ex:283 lib/graphql/resolvers/member.ex:83
#, elixir-format
msgid "Group not found"
msgstr "Grupo no encontrado"

#: lib/graphql/resolvers/group.ex:69
#, elixir-format
msgid "Group with ID %{id} not found"
msgstr "No se encontró el grupo con ID% {id}"

#: lib/graphql/resolvers/user.ex:83
#, elixir-format
msgid "Impossible to authenticate, either your email or password are invalid."
msgstr ""
"Imposible autenticarse, su correo electrónico o contraseña no son válidos."

#: lib/graphql/resolvers/group.ex:280
#, elixir-format
msgid "Member not found"
msgstr "Miembro no encontrado"

#: lib/graphql/resolvers/actor.ex:58 lib/graphql/resolvers/actor.ex:88
#: lib/graphql/resolvers/user.ex:417
#, elixir-format
msgid "No profile found for the moderator user"
msgstr "No se encontró el perfil del usuario moderador"

#: lib/graphql/resolvers/user.ex:195
#, elixir-format
msgid "No user to validate with this email was found"
msgstr "No se encontró ningún usuario para validar con este correo electrónico"

#: lib/graphql/resolvers/person.ex:232 lib/graphql/resolvers/user.ex:76
#: lib/graphql/resolvers/user.ex:219
#, elixir-format
msgid "No user with this email was found"
msgstr "No se encontró ningún usuario con este correo electrónico"

#: lib/graphql/resolvers/comment.ex:50 lib/graphql/resolvers/comment.ex:112
#: lib/graphql/resolvers/event.ex:286 lib/graphql/resolvers/feed_token.ex:28 lib/graphql/resolvers/group.ex:245
#: lib/graphql/resolvers/member.ex:80 lib/graphql/resolvers/participant.ex:29
#: lib/graphql/resolvers/participant.ex:163 lib/graphql/resolvers/participant.ex:192 lib/graphql/resolvers/person.ex:157
#: lib/graphql/resolvers/person.ex:191 lib/graphql/resolvers/person.ex:256 lib/graphql/resolvers/person.ex:288
#: lib/graphql/resolvers/person.ex:301 lib/graphql/resolvers/picture.ex:75 lib/graphql/resolvers/report.ex:110
#: lib/graphql/resolvers/todos.ex:57
#, elixir-format
msgid "Profile is not owned by authenticated user"
msgstr "El perfil no es propiedad del usuario autenticado"

#: lib/graphql/resolvers/user.ex:125
#, elixir-format
msgid "Registrations are not open"
msgstr "Las inscripciones no están abiertas"

#: lib/graphql/resolvers/user.ex:330
#, elixir-format
msgid "The current password is invalid"
msgstr "La contraseña actual no es válida"

#: lib/graphql/resolvers/user.ex:382
#, elixir-format
msgid "The new email doesn't seem to be valid"
msgstr "El nuevo correo electrónico no parece ser válido"

#: lib/graphql/resolvers/user.ex:379
#, elixir-format
msgid "The new email must be different"
msgstr "El nuevo correo electrónico debe ser diferente"

#: lib/graphql/resolvers/user.ex:333
#, elixir-format
msgid "The new password must be different"
msgstr "La nueva contraseña debe ser diferente"

#: lib/graphql/resolvers/user.ex:376 lib/graphql/resolvers/user.ex:439
#: lib/graphql/resolvers/user.ex:442
#, elixir-format
msgid "The password provided is invalid"
msgstr "La contraseña proporcionada no es válida"

#: lib/graphql/resolvers/user.ex:337
#, elixir-format
msgid "The password you have chosen is too short. Please make sure your password contains at least 6 characters."
msgstr ""
"La contraseña que ha elegido es demasiado corta. Asegúrese de que su "
"contraseña contenga al menos 6 caracteres."

#: lib/graphql/resolvers/user.ex:215
#, elixir-format
msgid "This user can't reset their password"
msgstr "Este usuario no puede restablecer su contraseña"

#: lib/graphql/resolvers/user.ex:79
#, elixir-format
msgid "This user has been disabled"
msgstr "Este usuario ha sido inhabilitado"

#: lib/graphql/resolvers/user.ex:179
#, elixir-format
msgid "Unable to validate user"
msgstr "No se puede validar al usuario"

#: lib/graphql/resolvers/user.ex:420
#, elixir-format
msgid "User already disabled"
msgstr "El usuario ya está inhabilitado"

#: lib/graphql/resolvers/user.ex:487
#, elixir-format
msgid "User requested is not logged-in"
msgstr "El usuario solicitado no ha iniciado sesión"

#: lib/graphql/resolvers/group.ex:254
#, elixir-format
msgid "You are already a member of this group"
msgstr "Ya eres miembro de este grupo"

#: lib/graphql/resolvers/group.ex:287
#, elixir-format
msgid "You can't leave this group because you are the only administrator"
msgstr "No puedes dejar este grupo porque eres el único administrador"

#: lib/graphql/resolvers/group.ex:251
#, elixir-format
msgid "You cannot join this group"
msgstr "No puedes unirte a este grupo"

#: lib/graphql/resolvers/group.ex:97
#, elixir-format
msgid "You may not list groups unless moderator."
msgstr "No puedes enumerar grupos a menos que seas moderador."

#: lib/graphql/resolvers/user.ex:387
#, elixir-format
msgid "You need to be logged-in to change your email"
msgstr "Debes iniciar sesión para cambiar tu correo electrónico"

#: lib/graphql/resolvers/user.ex:345
#, elixir-format
msgid "You need to be logged-in to change your password"
msgstr "Debes iniciar sesión para cambiar tu contraseña"

#: lib/graphql/resolvers/group.ex:212
#, elixir-format
msgid "You need to be logged-in to delete a group"
msgstr "Debes iniciar sesión para eliminar un grupo"

#: lib/graphql/resolvers/user.ex:447
#, elixir-format
msgid "You need to be logged-in to delete your account"
msgstr "Debes iniciar sesión para eliminar su cuenta"

#: lib/graphql/resolvers/group.ex:259
#, elixir-format
msgid "You need to be logged-in to join a group"
msgstr "Debes iniciar sesión para eliminar su cuenta"

#: lib/graphql/resolvers/group.ex:292
#, elixir-format
msgid "You need to be logged-in to leave a group"
msgstr "Debes iniciar sesión para dejar un grupo"

#: lib/graphql/resolvers/group.ex:177
#, elixir-format
msgid "You need to be logged-in to update a group"
msgstr "Debes iniciar sesión para actualizar un grupo"

#: lib/graphql/resolvers/user.ex:58
#, elixir-format
msgid "You need to have admin access to list users"
msgstr "Necesitas tener acceso de administrador para listar usuarios"

#: lib/graphql/resolvers/user.ex:108
#, elixir-format
msgid "You need to have an existing token to get a refresh token"
msgstr "Debes tener un token existente para obtener un token de actualización"

#: lib/graphql/resolvers/user.ex:198 lib/graphql/resolvers/user.ex:222
#, elixir-format
msgid "You requested again a confirmation email too soon"
msgstr ""
"Solicitó de nuevo un correo electrónico de confirmación demasiado pronto"

#: lib/graphql/resolvers/user.ex:128
#, elixir-format
msgid "Your email is not on the allowlist"
msgstr "Tu correo electrónico no está en la lista de permitidos"

#: lib/graphql/resolvers/actor.ex:64 lib/graphql/resolvers/actor.ex:94
#, elixir-format
msgid "Error while performing background task"
msgstr "Error al realizar la tarea en segundo plano"

#: lib/graphql/resolvers/actor.ex:27
#, elixir-format
msgid "No profile found with this ID"
msgstr "No se encontró ningún perfil con este ID"

#: lib/graphql/resolvers/actor.ex:54 lib/graphql/resolvers/actor.ex:91
#, elixir-format
msgid "No remote profile found with this ID"
msgstr "No se encontró ningún perfil remoto con este ID"

#: lib/graphql/resolvers/actor.ex:69
#, elixir-format
msgid "Only moderators and administrators can suspend a profile"
msgstr "Solo los moderadores y administradores pueden suspender un perfil"

#: lib/graphql/resolvers/actor.ex:99
#, elixir-format
msgid "Only moderators and administrators can unsuspend a profile"
msgstr ""
"Solo los moderadores y administradores pueden anular la suspensión de un "
"perfil"

#: lib/graphql/resolvers/actor.ex:24
#, elixir-format
msgid "Only remote profiles may be refreshed"
msgstr "Solo se pueden actualizar los perfiles remotos"

#: lib/graphql/resolvers/actor.ex:61
#, elixir-format
msgid "Profile already suspended"
msgstr "Perfil ya suspendido"

#: lib/graphql/resolvers/participant.ex:96
#, elixir-format
msgid "A valid email is required by your instance"
msgstr "Su instancia requiere un correo electrónico válido"

#: lib/graphql/resolvers/participant.ex:90
#, elixir-format
msgid "Anonymous participation is not enabled"
msgstr "La participación anónima no está habilitada"

#: lib/graphql/resolvers/person.ex:188
#, elixir-format
msgid "Cannot remove the last administrator of a group"
msgstr "No se puede eliminar al último administrador de un grupo"

#: lib/graphql/resolvers/person.ex:185
#, elixir-format
msgid "Cannot remove the last identity of a user"
msgstr "No se puede eliminar la última identidad de un usuario"

#: lib/graphql/resolvers/comment.ex:109
#, elixir-format
msgid "Comment is already deleted"
msgstr "El comentario ya está eliminado"

#: lib/graphql/resolvers/discussion.ex:61
#, elixir-format
msgid "Discussion not found"
msgstr "Discusión no encontrada"

#: lib/graphql/resolvers/report.ex:62 lib/graphql/resolvers/report.ex:87
#, elixir-format
msgid "Error while saving report"
msgstr "Error al guardar el informe"

#: lib/graphql/resolvers/report.ex:113
#, elixir-format
msgid "Error while updating report"
msgstr "Error al actualizar el informe"

#: lib/graphql/resolvers/participant.ex:131
#, elixir-format
msgid "Event id not found"
msgstr "ID de evento no encontrado"

#: lib/graphql/error.ex:89 lib/graphql/resolvers/event.ex:238
#: lib/graphql/resolvers/event.ex:283
#, elixir-format
msgid "Event not found"
msgstr "Evento no encontrado"

#: lib/graphql/resolvers/participant.ex:87
#: lib/graphql/resolvers/participant.ex:128 lib/graphql/resolvers/participant.ex:160
#, elixir-format
msgid "Event with this ID %{id} doesn't exist"
msgstr "El evento con este ID%{id} no existe"

#: lib/graphql/resolvers/participant.ex:103
#, elixir-format
msgid "Internal Error"
msgstr "Error interno"

#: lib/graphql/resolvers/event.ex:100 lib/graphql/resolvers/participant.ex:234
#, elixir-format
msgid "Moderator profile is not owned by authenticated user"
msgstr "El perfil del moderador no es propiedad del usuario autenticado"

#: lib/graphql/resolvers/discussion.ex:181
#, elixir-format
msgid "No discussion with ID %{id}"
msgstr "Sin discusión con ID%{id}"

#: lib/graphql/resolvers/todos.ex:81 lib/graphql/resolvers/todos.ex:171
#, elixir-format
msgid "No profile found for user"
msgstr "No se encontró perfil para el usuario"

#: lib/graphql/resolvers/feed_token.ex:66
#, elixir-format
msgid "No such feed token"
msgstr "No existe tal token de alimentación"

#: lib/graphql/resolvers/event.ex:202
#, elixir-format
msgid "Organizer profile is not owned by the user"
msgstr "El perfil del organizador no es propiedad del usuario"

#: lib/graphql/resolvers/participant.ex:244
#, elixir-format
msgid "Participant already has role %{role}"
msgstr "El participante ya tiene el rol%{role}"

#: lib/graphql/resolvers/participant.ex:173
#: lib/graphql/resolvers/participant.ex:202 lib/graphql/resolvers/participant.ex:237
#: lib/graphql/resolvers/participant.ex:247
#, elixir-format
msgid "Participant not found"
msgstr "Participante no encontrado"

#: lib/graphql/resolvers/person.ex:31
#, elixir-format
msgid "Person with ID %{id} not found"
msgstr "Persona con ID%{id} no encontrada"

#: lib/graphql/resolvers/person.ex:52
#, elixir-format
msgid "Person with username %{username} not found"
msgstr "Persona con nombre de usuario %{username} no encontrada"

#: lib/graphql/resolvers/picture.ex:45
#, elixir-format
msgid "Picture with ID %{id} was not found"
msgstr "No se encontró la foto con ID %{id}"

#: lib/graphql/resolvers/post.ex:165 lib/graphql/resolvers/post.ex:198
#, elixir-format
msgid "Post ID is not a valid ID"
msgstr "La ID de publicación no es válida"

#: lib/graphql/resolvers/post.ex:168 lib/graphql/resolvers/post.ex:201
#, elixir-format
msgid "Post doesn't exist"
msgstr "La publicación no existe"

#: lib/graphql/resolvers/member.ex:86
#, elixir-format
msgid "Profile invited doesn't exist"
msgstr "El perfil invitado no existe"

#: lib/graphql/resolvers/member.ex:95 lib/graphql/resolvers/member.ex:99
#, elixir-format
msgid "Profile is already a member of this group"
msgstr "Perfil ya es miembro de este grupo"

#: lib/graphql/resolvers/post.ex:131 lib/graphql/resolvers/post.ex:171
#: lib/graphql/resolvers/post.ex:204 lib/graphql/resolvers/resource.ex:87 lib/graphql/resolvers/resource.ex:124
#: lib/graphql/resolvers/resource.ex:153 lib/graphql/resolvers/resource.ex:182 lib/graphql/resolvers/todos.ex:60
#: lib/graphql/resolvers/todos.ex:84 lib/graphql/resolvers/todos.ex:102 lib/graphql/resolvers/todos.ex:174
#: lib/graphql/resolvers/todos.ex:197 lib/graphql/resolvers/todos.ex:225
#, elixir-format
msgid "Profile is not member of group"
msgstr "El perfil no es miembro del grupo"

#: lib/graphql/resolvers/person.ex:154 lib/graphql/resolvers/person.ex:182
#, elixir-format
msgid "Profile not found"
msgstr "Perfil no encontrado"

#: lib/graphql/resolvers/event.ex:104 lib/graphql/resolvers/participant.ex:241
#, elixir-format
msgid "Provided moderator profile doesn't have permission on this event"
msgstr "El perfil de moderador proporcionado no tiene permiso para este evento"

#: lib/graphql/resolvers/report.ex:38
#, elixir-format
msgid "Report not found"
msgstr "Informe no encontrado"

#: lib/graphql/resolvers/resource.ex:150 lib/graphql/resolvers/resource.ex:179
#, elixir-format
msgid "Resource doesn't exist"
msgstr "El recurso no existe"

#: lib/graphql/resolvers/participant.ex:124
#, elixir-format
msgid "The event has already reached its maximum capacity"
msgstr "El evento ya alcanzó su capacidad máxima"

#: lib/graphql/resolvers/participant.ex:267
#, elixir-format
msgid "This token is invalid"
msgstr "Este token no es válido"

#: lib/graphql/resolvers/todos.ex:168 lib/graphql/resolvers/todos.ex:222
#, elixir-format
msgid "Todo doesn't exist"
msgstr "Todo no existe"

#: lib/graphql/resolvers/todos.ex:78 lib/graphql/resolvers/todos.ex:194
#: lib/graphql/resolvers/todos.ex:219
#, elixir-format
msgid "Todo list doesn't exist"
msgstr "La lista de tareas pendientes no existe"

#: lib/graphql/resolvers/feed_token.ex:72
#, elixir-format
msgid "Token does not exist"
msgstr "El token no existe"

#: lib/graphql/resolvers/feed_token.ex:69
#, elixir-format
msgid "Token is not a valid UUID"
msgstr "El token no es un UUID válido"

#: lib/graphql/error.ex:87 lib/graphql/resolvers/person.ex:323
#, elixir-format
msgid "User not found"
msgstr "Usuario no encontrado"

#: lib/graphql/resolvers/person.ex:235
#, elixir-format
msgid "You already have a profile for this user"
msgstr "Ya tienes un perfil para este usuario"

#: lib/graphql/resolvers/participant.ex:134
#, elixir-format
msgid "You are already a participant of this event"
msgstr "Ya eres participante de este evento"

#: lib/graphql/resolvers/discussion.ex:185
#, elixir-format
msgid "You are not a member of the group the discussion belongs to"
msgstr "No eres miembro del grupo al que pertenece la discusión"

#: lib/graphql/resolvers/member.ex:89
#, elixir-format
msgid "You are not a member of this group"
msgstr "no eres un miembro de este grupo"

#: lib/graphql/resolvers/member.ex:154
#, elixir-format
msgid "You are not a moderator or admin for this group"
msgstr "No eres moderador ni administrador de este grupo"

#: lib/graphql/resolvers/comment.ex:55
#, elixir-format
msgid "You are not allowed to create a comment if not connected"
msgstr "No está permitido crear un comentario si no está conectado"

#: lib/graphql/resolvers/feed_token.ex:44
#, elixir-format
msgid "You are not allowed to create a feed token if not connected"
msgstr "No puede crear un token de feed si no está conectado"

#: lib/graphql/resolvers/comment.ex:117
#, elixir-format
msgid "You are not allowed to delete a comment if not connected"
msgstr "No puede eliminar un comentario si no está conectado"

#: lib/graphql/resolvers/feed_token.ex:81
#, elixir-format
msgid "You are not allowed to delete a feed token if not connected"
msgstr "No puede eliminar un token de feed si no está conectado"

#: lib/graphql/resolvers/comment.ex:77
#, elixir-format
msgid "You are not allowed to update a comment if not connected"
msgstr "No se le permite actualizar un comentario si no está conectado"

#: lib/graphql/resolvers/participant.ex:167
#: lib/graphql/resolvers/participant.ex:196
#, elixir-format
msgid "You can't leave event because you're the only event creator participant"
msgstr ""
"No puedes abandonar el evento porque eres el único participante creador del "
"evento"

#: lib/graphql/resolvers/member.ex:158
#, elixir-format
msgid "You can't set yourself to a lower member role for this group because you are the only administrator"
msgstr ""
"No puede establecerse en un rol de miembro inferior para este grupo porque "
"es el único administrador"

#: lib/graphql/resolvers/comment.ex:105
#, elixir-format
msgid "You cannot delete this comment"
msgstr "No puedes borrar este comentario"

#: lib/graphql/resolvers/event.ex:279
#, elixir-format
msgid "You cannot delete this event"
msgstr "No puedes borrar este evento"

#: lib/graphql/resolvers/member.ex:92
#, elixir-format
msgid "You cannot invite to this group"
msgstr "No puedes invitar a este grupo"

#: lib/graphql/resolvers/feed_token.ex:75
#, elixir-format
msgid "You don't have permission to delete this token"
msgstr "No tienes permiso para eliminar este token"

#: lib/graphql/resolvers/admin.ex:52
#, elixir-format
msgid "You need to be logged-in and a moderator to list action logs"
msgstr ""
"Debe iniciar sesión y un moderador para enumerar los registros de acción"

#: lib/graphql/resolvers/report.ex:28
#, elixir-format
msgid "You need to be logged-in and a moderator to list reports"
msgstr "Debe iniciar sesión y un moderador para enumerar los informes"

#: lib/graphql/resolvers/report.ex:118
#, elixir-format
msgid "You need to be logged-in and a moderator to update a report"
msgstr "Debe iniciar sesión y ser un moderador para actualizar un informe"

#: lib/graphql/resolvers/report.ex:43
#, elixir-format
msgid "You need to be logged-in and a moderator to view a report"
msgstr "Debe iniciar sesión y ser un moderador para actualizar un informe"

#: lib/graphql/resolvers/admin.ex:220
#, elixir-format
msgid "You need to be logged-in and an administrator to access admin settings"
msgstr ""
"Debe iniciar sesión y ser administrador para acceder a la configuración de "
"administrador"

#: lib/graphql/resolvers/admin.ex:205
#, elixir-format
msgid "You need to be logged-in and an administrator to access dashboard statistics"
msgstr ""
"Debe iniciar sesión y ser administrador para acceder a las estadísticas del "
"panel"

#: lib/graphql/resolvers/admin.ex:244
#, elixir-format
msgid "You need to be logged-in and an administrator to save admin settings"
msgstr ""
"Debe iniciar sesión y ser administrador para acceder a las estadísticas del "
"panel"

#: lib/graphql/resolvers/discussion.ex:66
#, elixir-format
msgid "You need to be logged-in to access discussions"
msgstr "Debe iniciar sesión para acceder a las discusiones"

#: lib/graphql/resolvers/resource.ex:93
#, elixir-format
msgid "You need to be logged-in to access resources"
msgstr "Debes iniciar sesión para acceder a los recursos"

#: lib/graphql/resolvers/event.ex:213
#, elixir-format
msgid "You need to be logged-in to create events"
msgstr "Debes iniciar sesión para crear eventos"

#: lib/graphql/resolvers/post.ex:139
#, elixir-format
msgid "You need to be logged-in to create posts"
msgstr "Debes iniciar sesión para crear publicaciones"

#: lib/graphql/resolvers/report.ex:81 lib/graphql/resolvers/report.ex:92
#, elixir-format
msgid "You need to be logged-in to create reports"
msgstr "Debe iniciar sesión para crear informes"

#: lib/graphql/resolvers/resource.ex:129
#, elixir-format
msgid "You need to be logged-in to create resources"
msgstr "Debe iniciar sesión para crear recursos"

#: lib/graphql/resolvers/event.ex:291
#, elixir-format
msgid "You need to be logged-in to delete an event"
msgstr "Debe iniciar sesión para eliminar un evento"

#: lib/graphql/resolvers/post.ex:209
#, elixir-format
msgid "You need to be logged-in to delete posts"
msgstr "Debes iniciar sesión para eliminar publicaciones"

#: lib/graphql/resolvers/resource.ex:187
#, elixir-format
msgid "You need to be logged-in to delete resources"
msgstr "Debes iniciar sesión para eliminar recursos"

#: lib/graphql/resolvers/participant.ex:108
#, elixir-format
msgid "You need to be logged-in to join an event"
msgstr "Debes iniciar sesión para eliminar recursos"

#: lib/graphql/resolvers/participant.ex:207
#, elixir-format
msgid "You need to be logged-in to leave an event"
msgstr "Debes iniciar sesión para salir de un evento"

#: lib/graphql/resolvers/event.ex:252
#, elixir-format
msgid "You need to be logged-in to update an event"
msgstr "Debe iniciar sesión para actualizar un evento"

#: lib/graphql/resolvers/post.ex:176
#, elixir-format
msgid "You need to be logged-in to update posts"
msgstr "Debes iniciar sesión para actualizar las publicaciones"

#: lib/graphql/resolvers/resource.ex:158
#, elixir-format
msgid "You need to be logged-in to update resources"
msgstr "Debes iniciar sesión para actualizar los recursos"

#: lib/graphql/resolvers/resource.ex:210
#, elixir-format
msgid "You need to be logged-in to view a resource preview"
msgstr "Debe iniciar sesión para ver una vista previa del recurso"

#: lib/graphql/resolvers/picture.ex:86
#, elixir-format
msgid "You need to login to upload a picture"
msgstr "Debes iniciar sesión para subir una imagen"

#: lib/graphql/resolvers/report.ex:84
#, elixir-format
msgid "Reporter ID does not match the anonymous profile id"
msgstr ""
"La identificación del informante no coincide con la identificación del "
"perfil anónimo"

#: lib/graphql/resolvers/report.ex:59
#, elixir-format
msgid "Reporter profile is not owned by authenticated user"
msgstr "El perfil del denunciante no es propiedad de un usuario autenticado"

#: lib/graphql/resolvers/resource.ex:121
#, elixir-format
msgid "Parent resource doesn't belong to this group"
msgstr "El recurso principal no pertenece a este grupo"

#: lib/graphql/resolvers/participant.ex:93
#, elixir-format
msgid "Profile ID provided is not the anonymous profile one"
msgstr "El ID de perfil proporcionado no es el del perfil anónimo"

#: lib/mobilizon/users/user.ex:109
#, elixir-format
msgid "The chosen password is too short."
msgstr "La contraseña elegida es demasiado corta."

#: lib/mobilizon/users/user.ex:138
#, elixir-format
msgid "The registration token is already in use, this looks like an issue on our side."
msgstr ""
"El token de registro ya está en uso, esto parece un problema de nuestra "
"parte."

#: lib/mobilizon/users/user.ex:104
#, elixir-format
msgid "This email is already used."
msgstr "Este correo electrónico ya está en uso."

#: lib/graphql/error.ex:88
#, elixir-format
msgid "Post not found"
msgstr "Informe no encontrado"

#: lib/graphql/error.ex:75
#, elixir-format
msgid "Invalid arguments passed"
msgstr "Se pasaron argumentos no válidos"

#: lib/graphql/error.ex:81
#, elixir-format
msgid "Invalid credentials"
msgstr "Credenciales no válidas"

#: lib/graphql/error.ex:79
#, elixir-format
msgid "Reset your password to login"
msgstr "Restablezca su contraseña para iniciar sesión"

#: lib/graphql/error.ex:86 lib/graphql/error.ex:91
#, elixir-format
msgid "Resource not found"
msgstr "Recurso no encontrado"

#: lib/graphql/error.ex:92
#, elixir-format
msgid "Something went wrong"
msgstr "Algo salió mal"

#: lib/graphql/error.ex:74
#, elixir-format
msgid "Unknown Resource"
msgstr "Recurso desconocido"

#: lib/graphql/error.ex:84
#, elixir-format
msgid "You don't have permission to do this"
msgstr "No tienes permiso para hacer esto"

#: lib/graphql/error.ex:76
#, elixir-format
msgid "You need to be logged in"
msgstr "Debes iniciar sesión"

#: lib/graphql/resolvers/member.ex:119
#, elixir-format
msgid "You can't accept this invitation with this profile."
msgstr "No puedes aceptar esta invitación con este perfil."

#: lib/graphql/resolvers/member.ex:137
#, elixir-format
msgid "You can't reject this invitation with this profile."
msgstr "No puedes rechazar esta invitación con este perfil."

#: lib/graphql/resolvers/picture.ex:78
#, elixir-format
msgid "File doesn't have an allowed MIME type."
msgstr "El archivo no tiene un tipo MIME permitido."

#: lib/graphql/resolvers/group.ex:172
#, elixir-format
msgid "Profile is not administrator for the group"
msgstr "El perfil no es miembro del grupo"

#: lib/graphql/resolvers/event.ex:241
#, elixir-format
msgid "You can't edit this event."
msgstr "No puedes borrar este evento."

#: lib/graphql/resolvers/event.ex:244
#, elixir-format
msgid "You can't attribute this event to this profile."
msgstr "No puedes rechazar esta invitación con este perfil."

#: lib/graphql/resolvers/member.ex:140
#, elixir-format
msgid "This invitation doesn't exist."
msgstr "Esta invitación no existe."

#: lib/graphql/resolvers/member.ex:182
#, elixir-format
msgid "This member already has been rejected."
msgstr "Este miembro ya ha sido rechazado."

#: lib/graphql/resolvers/member.ex:189
#, elixir-format
msgid "You don't have the right to remove this member."
msgstr "No tiene derecho a eliminar este miembro."
